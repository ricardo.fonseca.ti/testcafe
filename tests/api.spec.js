import http from 'http';
import https from 'https';

fixture`Test REST API`;

test('Simple Test', async t => {
    const response = await getResponseData('https://reqres.in/api/users/2');
    const responseBody = JSON.parse(response.rawData);

    await t
        .expect(response.statusCode).eql(200)
        .expect(responseBody.data.id).eql(2);
});

const getResponseData = (url) => new Promise((resolve, reject) => {
    const client = (new URL(url).protocol.includes("https")) ? https : http;

    client.get(url, res => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];

        res.setEncoding('utf8');

        let rawData = '';

        res.on('data', (chunk) => { rawData += chunk; });
        res.on('end', () => resolve({ statusCode, contentType, rawData }));

    }).on('error', e => reject(e));
});